# Write a program which takes input from keyboard and checks the lenght of the given input. 
# If it is less than 16 and greater than 4 print 'ok', otherwise print 'ko'

user_input = raw_input("Type something: ")

def input_check(word):
    return len(word) < 16 and len(word) > 4
     
if input_check(user_input):
    print 'ok'
else:
    print 'ko'

