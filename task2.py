# Write a program which takes a full name as input from keyboard,
# checks if the first letter from the names is lowercase and if it
# is not, capitalize it and print the result

user_input = raw_input("Please type your full name: ")

def first_letter_check(full_name):
    """ Returns every name with its first character capitalized """
    name_list = full_name.split(' ')
    return ' '.join([name[0].upper()+name[1:] for name in name_list])
    
def name_check(full_name):
    """ Returns every name with its first character capitalized and the rest lowercased. """ 
    return full_name.title()
    
name = first_letter_check(user_input)
print name
        
name = name_check(user_input)
print name
       
    
