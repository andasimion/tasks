# Given 2 arrays of integers, a and b, write a function which will 
# return true if they have the same first element or they have the
# the same last element, otherwise will return false.
# Both arrays will be lenght 1 or more 
# If the output of the function is true print 'ok', else print 'ko'

def list_check(lst1, lst2):
    return (len(lst1)>=1 and len(lst2)>=1) and (lst1[0] == lst2[0] or lst1[-1] == lst2[-1])

a = [1, 2, 3, 4]
b = [4]
    
if list_check(a, b):
    print 'ok'
else:
    print 'ko'
    
