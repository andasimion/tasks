# Write a program which reads a phrase/line from keyboard and   
# prints the number of times that the string "Pen\taStagiu" 
# appears anywhere in the given string.
# Accept any letter for the 'e'. So "Pin\taStagiu', count too.
# "\" is the literal backslash character, not the escape sequence

import re

user_input = raw_input("Type something here: ")

def search_phrase(phrase):
    pattern = re.findall(r'P[a-zA-Z]n\\taStagiu', phrase)  # pattern is a list
    return len(pattern)
    
a = search_phrase(user_input)
print a

